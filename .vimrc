let mapleader=";"
" set pastetoggle=<F10>  " Bind `F10` to `:set paste`
set pastetoggle=<leader>p

set nocompatible  " Use the vim's keyboard setting, not vi
