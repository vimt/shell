export ZSH="/root/.oh-my-zsh"
ZSH_THEME="ys"
plugins=(git autojump extract)
source $ZSH/oh-my-zsh.sh
HOME=${HOME:=~}

if [ -f $HOME/.shells/alias ]; then
    RCDIR=$HOME/.shells
else
    RCDIR=$HOME/.shells/rc
fi
source $RCDIR/functions
source $RCDIR/alias
