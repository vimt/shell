

### How to use
```shell script
git clone --depth=1 https://gitlab.com/vimt/shell.git ~/.shells
~/.shells/ultimate_vimrc.sh
~/.shells/install.sh
```
