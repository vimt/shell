#!/usr/bin/env bash

cd ~/.shells && git pull

[ -f ~/.bashrc ] && rm ~/.bashrc
[ -f ~/.profile ] && rm ~/.profile
[ -f ~/.vimrc ] && rm ~/.vimrc

ln -sf ~/.shells/.bashrc ~/.bashrc
ln -sf ~/.shells/.zshrc ~/.zshrc
ln -sf ~/.shells/.profile ~/.profile
